import static org.junit.Assert.*;

import org.junit.Test;


public class TestSaving {
	SavingAcc SavingAcc = new SavingAcc() ;

	@Test
	public void testCalculateAmount() {
		SavingAcc.CalculateAmount(100,20) ;
		assertTrue(SavingAcc.getAmount() == 120) ;
	}

	
	@Test
	public void testPayForLoan() {
		SavingAcc.PayForLoan(1000) ;
		assertTrue(SavingAcc.getBalance() == 800) ;
	}
	

	@Test
	public void testPayForWallet() {
		SavingAcc.PayForWallet(10);
		assertTrue(SavingAcc.getAmountForWallet() == 790) ;
		
	}

}
