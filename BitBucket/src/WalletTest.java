import static org.junit.Assert.*;

import org.junit.Test;


public class WalletTest {
	Wallet Wallet = new Wallet() ;

	@Test
	public void testGetdeducted_amount() {
		
		Wallet.Tick("Guitar");
		assertTrue(Wallet.getdeducted_amount() == (50)) ;
	}

	
	@Test
	public void testGetdeducted_amount2() {
		
		Wallet.Tick("Mobile");
		assertTrue(Wallet.getdeducted_amount() == (10)) ;
	}

	@Test
	public void testGetdeducted_amount3() {
		
		Wallet.Tick(" ");
		assertTrue(Wallet.getdeducted_amount() == (10)) ;
	}
	

	
	
	@Test
	public void testAdd_Items() {
		Wallet.AddItems("Guitar") ;
		assertTrue(Wallet.getItems().equals("[Guitar]")) ;
		
	}
	
	@Test
	public void testAllowToDeduct() {
		Wallet.AllowToDeduct(10,25) ;
		assertTrue(Wallet.getAllowToDeduct() == true) ;
		
		
	}
	
	
	
	@Test
	public void testDelete_Items() {
		Wallet.DeleteItems("Guitar") ;
		assertTrue(Wallet.getItems().equals(" ")) ;
		
		
	}
	
	
	
}
