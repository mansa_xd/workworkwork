import static org.junit.Assert.*;

import org.junit.Test;


public class TestIncome {
	
	income income = new income();

	
	
	@Test
	public void testSetTax() throws Exception {
		income.setTax(20.00,"May1");
		
		assertTrue(income.getTax() == (20.00)) ;
		
	}

	
	@Test
	public void testSetSuperannuation() throws Exception {
        income.setSuperannuation(5.20,"June3");
		
		assertTrue(income.getSuperannuation() == (5.20)) ;  
	}

	

	@Test
	public void testSetSalary() throws Exception {
		 income.setSalary(200,false,"May1");
			
			assertTrue(income.getSalary() == (200)) ;  
	}

	
	@Test
	public void testSetSalary2() throws Exception {
		 income.setSalary(100,false,"June1");
			
			assertTrue(income.getSalary() == (100)) ;  
	}

	@Test
	public void testSetLowestSalary() throws Exception {
		 income.setLowestSalary(new int[]{100,25,40},"June1");
			
			assertTrue(income.getlowestSalary() == (25)) ;  
	}
	
	

}
