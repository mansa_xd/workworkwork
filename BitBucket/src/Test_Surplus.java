import static org.junit.Assert.*;

import org.junit.Test;


public class Test_Surplus {
	
	Surplus Surplus;

	
	@Test
	public void DataConstructsFromInitialItems() {
		assertNotNull(new Surplus());
	}

	@Test
	public void testSetOvertime() throws Exception  {
		
		Surplus.setOvertime(68.00,"Feb1");
		
		assertTrue(Surplus.getOvertime() == (68.0)) ;
	}

	@Test
	public void testSetBonus() throws Exception {
		Surplus.setBonus(68.0,"Jan1");
		
				assertTrue(Surplus.getBonus() == (68.0)) ;
		
	}

	

}