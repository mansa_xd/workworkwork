import static org.junit.Assert.*;

import org.junit.Test;


public class TestExpenditure {
	

	Expenditure expenditure = new Expenditure();
	
	
	@Test
	public void DataConstructsFromInitialItems() {
		assertNotNull(new Surplus());
	}

	
	
	
	
	@Test
	public void testGetInsurance() throws Exception {
		expenditure.setInsurance(68.00,"Sept1");
	
			assertTrue(expenditure.getInsurance() == (68.00)) ;
			
	}
	
	@Test
	public void testGetElectricity() throws Exception {
		expenditure.setInsurance(15.00,"Sept1");
	
			assertTrue(expenditure.getElectricity() == (15.00)) ;
			
	}

	
	@Test
	public void testGetsanityAllowence() throws Exception {
		expenditure.setSanityAllowence(100.00,"Sept1") ;
		
	
		assertTrue(expenditure.getsanityAllowence() == (30.00)) ;
		
}

}
